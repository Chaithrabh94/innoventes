import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TableData from './TableData';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      roomsCount: 1,
      adultsCount: 1,
      childrenCount: 0
    }
    this.increment = this.increment.bind(this)
  }

  increment(letter) {
    var totalCapacity = this.state.adultsCount + this.state.childrenCount
    switch(letter){
      case 'ri':
      if(this.state.roomsCount <= 4)
      this.setState({
        roomsCount: this.state.roomsCount + 1
      });
      break;

      case 'rd':
      if(this.state.roomsCount > 1)
      this.setState({
        roomsCount: this.state.roomsCount - 1
      });
      break;

      case 'ai':
      if(totalCapacity < this.state.roomsCount * 4)
      this.setState({
        adultsCount : this.state.adultsCount + 1
      })
      else
      alert('get one more room')
      break;

      case 'ad':
      if(this.state.adultsCount > 1)
      this.setState({
        adultsCount : this.state.adultsCount - 1
      })
      break;

      case 'ci':
      if(totalCapacity < this.state.roomsCount * 4)
      this.setState({
        childrenCount : this.state.childrenCount + 1
      })
      else
      alert('get one more room')
      break;

      case 'cd':
      if(this.state.childrenCount > 0)
      this.setState({
        childrenCount : this.state.childrenCount - 1
      })
      break;
    }
   
    console.log('2. Received click in App');
  }
  render() {
    return (
      <div className="App">
        <i class="fa fa-users color-blue" aria-hidden="true"></i><span className="fs-18 color-blue"> Choose number of <b>
          people</b></span>
        <table>
          <TableData clickInc={() => this.increment('ri')} clickDec= {() => this.increment('rd')} icon="fa fa-bed color-blue" name='Rooms' count={this.state.roomsCount} />
          <TableData clickInc={() => this.increment('ai')} clickDec= {() => this.increment('ad')} icon="fa fa-user color-blue" name='Adults' count={this.state.adultsCount} />
          <TableData clickInc={() => this.increment('ci')} clickDec= {() => this.increment('cd')} icon="fa fa-child color-blue" name='Children' count={this.state.childrenCount} />
        </table>
      <br/>
      <br/>
      

        <h3>Occupied Status</h3>
        <table>
          <tr>
            <th>Rooms</th>
            <th>Adults</th>
            <th>Children</th>
          </tr>
          <tr>
            <th>
              {this.state.roomsCount}
            </th>
            <th>
              {this.state.adultsCount}
            </th>
            <th>
              {this.state.childrenCount}
            </th>
          </tr>
        </table>
      </div>
    );
  }
}

export default App;
