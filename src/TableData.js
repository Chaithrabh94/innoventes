import React, { Component } from 'react';
import './App.css';

class TableData extends Component {
    constructor(props){
        super(props);

    }
  render() {
    return (
      <div>
            <tr>
              <td><i className={this.props.icon}></i> {this.props.name}</td>
              <td className="float-right"><i onClick={this.props.clickDec} className="fa fa-minus-circle fa-lg color-lightblue"></i> {this.props.count} 
              <i onClick={this.props.clickInc} className="fa fa-plus-circle fa-lg color-pink"></i></td>            
            </tr>
            <hr/>
      </div>
    );
  }
}

export default TableData;

